# UXF Core

## Cookie

- Cookie options
    - `secure?: boolean;`
    - `httpOnly?: boolean;`
    - `path?: string;`

```tsx
import { Cookie } from "@uxf/core/cookie";

// on client
const cookie = Cookie.create();

// in getInitialProps
const cookie = Cookie.create(ctx);

cookie.has("cookie-name");
cookie.get("cookie-name");
cookie.set("cookie-name", "value", /* ttl in seconds (optional) */, /* options (optional) */)
cookie.delete("cookie-name", /* options (optional) */);
```

## Hooks

```tsx
import { useBodyScrollLock } from "@uxf/core/hooks/useBodyScrollLock";

const innerRef = useRef<HTMLDivElement>(null);
const [isOpen, setIsOpen] = useState<boolean>();

const clearAllOnclose = false; /* optionally call clearAllBodyScrollLocks methot on unmount */

useBodyScrollLock<HTMLDivElement>(innerRef, isOpen, clearAllOnclose, { /* options from "body-scroll-lock" npm package */ });

<div ref={innerRef}>Element which activates scroll lock on its parent elements.</div>
```
```tsx
import { useIsMounted } from "@uxf/core/hooks/useIsMounted";

const isMounted = useIsMounted();
```
```tsx
import { useIsomorphicLayoutEffect } from "@uxf/core/hooks/useIsomorphicLayoutEffect";

useIsomorphicLayoutEffect(() => {/* code */}, [/* deps */]);
```

```tsx
import { useKey } from "@uxf/core/hooks/useKey";
import { useCallback } from "react";

const innerRef = useRef<HTMLDivElement>(null);
const disabled = false; // eg. for passing disabled state

useKey<HTMLDivElement>(innerRef, "Enter", () => console.log("callback"), "keydown", disabled);

<div ref={innerRef} tabIndex={0}>Element with callback triggerable by enter key.</div>
```
```tsx
import { useKeydownToMouseClick } from "@uxf/core/hooks/useKeydownToMouseClick";

const innerRef = useRef<HTMLDivElement>(null);
const disabled = false; // eg. for passing disabled state

useKeydownToMouseClick<HTMLDivElement>(innerRef, (e: KeyboardEvent) => e.key === "Enter", disabled);

<div onClick={() => console.log("success")} ref={innerRef}>Element with onClick triggerable both by mouse click and enter key.</div>
```
```tsx
import { useMinWindowWidth } from "@uxf/core/hooks/useMinWindowWidth";

const isDesktop = useMinWindowWidth(1200);

const example = isDesktop ? "desktop" : "tablet";
```

```ts
import { usePagination } from "@uxf/core/hooks/usePagination";

const paginationItems = usePagination({ page: 1, count: 10 })
```

```tsx
import { useRafState } from "@uxf/core/hooks/useRafState";

const [state, setState] = useRafState<boolean>(false);
```
```tsx
import { useUnmount } from "@uxf/core/hooks/useUnmount";

const exampleCallback = () => {};

useUnmount(exampleCallback());
```
```tsx
import { useUpdateEffect } from "@uxf/core/hooks/useUpdateEffect";

useUpdateEffect(() => {/* code */}, [/* deps */]);
```
```tsx
import { useWindowScroll } from "@uxf/core/hooks/useWindowScroll";

const windowScroll = useWindowScroll();

const example = windowScroll && windowScroll.y > 100 ? "scroled" : "on top";
```
```tsx
import { useWindowSize } from "@uxf/core/hooks/useWindowSize";

const windowSize = useWindowSize();

const example = windowSize && windowSize.width > 1200 ? "desktop" : "tablet";
```

## Next
```tsx
import { queryParamToString, queryParamToNumber } from "@uxf/core/next";

queryParamToNumber(ctx.query.id);
queryParamToString(ctx.query.name);
```

## Utils
```tsx
import { cameCaseToDash } from "@uxf/core/utils/cameCaseToDash";

const example = cameCaseToDash("fooBar") /* returns "foo-bar" */
```
```tsx
import { composeRefs } from "@uxf/core/utils/composeRefs";

const firstRef = useRef<HTMLDivElement>(null);
const secondRef = useRef<HTMLDivElement>(null);

const example = <div ref={composeRefs(firstRef, secondRef)} />;
```
```tsx
import { isBrowser } from "@uxf/core/utils/isBrowser";
import { isServer } from "@uxf/core/utils/isServer";

const browserExample = isBrowser /* returns true if DOM is available */
const serverExample = isServer /* returns true if DOM is NOT available */
```
```tsx
import { slugify } from "@uxf/core/utils/slugify";

const example = slugify("Jak se dnes máte?") /* returns "jak-se-dnes-mate" */
```
```tsx
import { trimTrailingZeros } from "@uxf/core/utils/trimTrailingZeros";

const example = trimTrailingZeros("120,450") /* returns "120,45" */
```

## Validators
```tsx
import { Validator } from "@uxf/core";

Validator.isEmail("...");
Validator.isPhone("...");
```
