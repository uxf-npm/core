import { paginationGenerate, PaginationConfig } from "./pagination";

const DATA: Array<[PaginationConfig, string]> = [
    [{ page: 1, count: 10 }, "previous 1 2 3 4 5 end-ellipsis 10 next"],
    [{ page: 2, count: 10 }, "previous 1 2 3 4 5 end-ellipsis 10 next"],
    [{ page: 3, count: 10 }, "previous 1 2 3 4 5 end-ellipsis 10 next"],
    [{ page: 4, count: 10 }, "previous 1 2 3 4 5 end-ellipsis 10 next"],
    [{ page: 5, count: 10 }, "previous 1 start-ellipsis 4 5 6 end-ellipsis 10 next"],
    [{ page: 6, count: 10 }, "previous 1 start-ellipsis 5 6 7 end-ellipsis 10 next"],
    [{ page: 7, count: 10 }, "previous 1 start-ellipsis 6 7 8 9 10 next"],
    [{ page: 8, count: 10 }, "previous 1 start-ellipsis 6 7 8 9 10 next"],
    [{ page: 9, count: 10 }, "previous 1 start-ellipsis 6 7 8 9 10 next"],
    [{ page: 10, count: 10 }, "previous 1 start-ellipsis 6 7 8 9 10 next"],
];

test("Pagination", () => {
    for (let [config, expected] of DATA) {
        expect(paginationGenerate(config).join(" ")).toBe(expected);
    }
});
