export function camelCaseToDash(content: string) {
    return content.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase();
}
