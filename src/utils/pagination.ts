export type PaginationConfig = {
    showFirstButton?: boolean;
    hidePrevButton?: boolean;
    boundaryCount?: number;
    siblingCount?: number;
    hideNextButton?: boolean;
    showLastButton?: boolean;
    page?: number;
    count?: number;
};
export type PaginationItem = "first" | "previous" | "next" | "last" | "start-ellipsis" | "end-ellipsis" | number;

const FIRST: PaginationItem[] = ["first"];
const LAST: PaginationItem[] = ["last"];
const PREVIOUS: PaginationItem[] = ["previous"];
const NEXT: PaginationItem[] = ["next"];
const START_ELLIPSIS: PaginationItem[] = ["start-ellipsis"];
const END_ELLIPSIS: PaginationItem[] = ["end-ellipsis"];

// @see https://github.com/mui-org/material-ui/blob/next/packages/material-ui/src/usePagination/usePagination.js
export const paginationGenerate = (config: PaginationConfig): PaginationItem[] => {
    const {
        showFirstButton = false,
        hidePrevButton = false,
        boundaryCount = 1,
        siblingCount = 1,
        showLastButton = false,
        hideNextButton = false,
        count = 1,
        page = 1,
    } = config;

    // https://dev.to/namirsab/comment/2050
    const range = (start: number, end: number): number[] => {
        const length = end - start + 1;
        return Array.from({ length }, (_, i) => start + i);
    };

    const startPages = range(1, Math.min(boundaryCount, count));
    const endPages = range(Math.max(count - boundaryCount + 1, boundaryCount + 1), count);

    const siblingsStart = Math.max(
        Math.min(
            // Natural start
            page - siblingCount,
            // Lower boundary when page is high
            count - boundaryCount - siblingCount * 2 - 1,
        ),
        // Greater than startPages
        boundaryCount + 2,
    );

    const siblingsEnd = Math.min(
        Math.max(
            // Natural end
            page + siblingCount,
            // Upper boundary when page is low
            boundaryCount + siblingCount * 2 + 2,
        ),
        // Less than endPages
        endPages.length > 0 ? endPages[0] - 2 : count - 1,
    );

    // Basic list of items to render
    // e.g. itemList = ['first', 'previous', 1, 'ellipsis', 4, 5, 6, 'ellipsis', 10, 'next', 'last']
    return [
        ...(showFirstButton ? FIRST : []),
        ...(hidePrevButton ? [] : PREVIOUS),
        ...startPages,

        // Start ellipsis
        // eslint-disable-next-line no-nested-ternary
        ...(siblingsStart > boundaryCount + 2
            ? START_ELLIPSIS
            : boundaryCount + 1 < count - boundaryCount
            ? [boundaryCount + 1]
            : []),

        // Sibling pages
        ...range(siblingsStart, siblingsEnd),

        // End ellipsis
        // eslint-disable-next-line no-nested-ternary
        ...(siblingsEnd < count - boundaryCount - 1
            ? END_ELLIPSIS
            : count - boundaryCount > boundaryCount
            ? [count - boundaryCount]
            : []),

        ...endPages,
        ...(hideNextButton ? [] : NEXT),
        ...(showLastButton ? LAST : []),
    ];
};
