export function trimTrailingZeros(value: string) {
    return value.replace(/\.?0+$/, "");
}
