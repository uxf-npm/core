interface CookieOptions {
    secure?: boolean;
    httpOnly?: boolean;
    path?: string;
}

export class Cookie {
    private ctx: any;

    constructor(ctx: any | null = null) {
        this.ctx = ctx;
    }

    public static create(ctx: any = null): Cookie {
        return new Cookie(ctx);
    }

    public has(name: string): boolean {
        return !!this.get(name);
    }

    public set(name: string, value: string, ttl: number = 3600, options: CookieOptions = {}): Cookie {
        const date = new Date();
        date.setTime(date.getTime() + ttl * 1000);

        const cookieParts = [`${name}=${encodeURIComponent(value)}`, `expires=${date.toUTCString()}`];
        if (options.httpOnly) {
            cookieParts.push("httponly");
        }
        if (options.secure) {
            cookieParts.push("secure");
        }
        cookieParts.push(`path=${options.path || "/"}`);
        const cookie = cookieParts.join("; ");

        if (typeof document !== "undefined") {
            document.cookie = cookie;
        } else {
            const existsCookies = (this.ctx.res.getHeader("Set-Cookie") || []).slice(0);
            existsCookies.push(cookie);
            this.ctx.res.setHeader("Set-Cookie", existsCookies);
        }

        return this;
    }

    public get(cookieName: string): string {
        const decodedCookie =
            typeof document !== "undefined"
                ? decodeURIComponent(document.cookie)
                : decodeURIComponent(this.ctx?.req?.headers?.cookie ?? "");

        const name = cookieName + "=";
        const cookies = decodedCookie.split(";");
        for (let cookie of cookies) {
            // tslint:disable-next-line:triple-equals
            while (cookie.charAt(0) == " ") {
                cookie = cookie.substring(1);
            }
            // tslint:disable-next-line:triple-equals
            if (cookie.indexOf(name) == 0) {
                return cookie.substring(name.length, cookie.length);
            }
        }

        return "";
    }

    public delete(name: string, options: CookieOptions = {}): Cookie {
        this.set(name, "", -1, options);
        return this;
    }
}
