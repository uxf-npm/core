// Email
const EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const isEmail = (email: string): boolean => EMAIL_REGEXP.test(email);

// Phone
const PHONE_REGEXP = /^(\+\d{1,3}|00\d{1,3})? ?[1-9][0-9]{2} ?[0-9]{3} ?[0-9]{3}$/;
const isPhone = (phone: string): boolean => PHONE_REGEXP.test(phone);

export const Validator = {
    isEmail,
    isPhone,
};
