import { Validator } from "./Validator";

test("isEmail", () => {
    expect(Validator.isEmail("dev@uxf.cz")).toBe(true);
    expect(Validator.isEmail("dev@uxf.c")).toBe(false);
    expect(Validator.isEmail("dev@uxf@c")).toBe(false);
});

test("isPhone", () => {
    expect(Validator.isPhone("+420777000111")).toBe(true);
    expect(Validator.isPhone("00420777000111")).toBe(true);
    expect(Validator.isPhone("777000111")).toBe(true);
    expect(Validator.isPhone("77700011")).toBe(false);
    expect(Validator.isPhone("+1777000111")).toBe(true);
    expect(Validator.isPhone("+22777000111")).toBe(true);
    expect(Validator.isPhone("+001777000111")).toBe(true);
});
