import { FormValidator } from "./FormValidator";

test("required", () => {
    expect(FormValidator.required("error")("not empty")).toBeUndefined();
    expect(FormValidator.required("error")(0)).toBeUndefined();
    expect(FormValidator.required("error")(false)).toBeUndefined();

    expect(FormValidator.required("error")("")).toBe("error");
    expect(FormValidator.required("error")(null)).toBe("error");
    expect(FormValidator.required("error")(undefined)).toBe("error");
});

test("isEmail", () => {
    expect(FormValidator.isEmail("error")("")).toBeUndefined();
    expect(FormValidator.isEmail("error")("dev@uxf.cz")).toBeUndefined();
    expect(FormValidator.isEmail("error")("dev@uxfcz")).toBe("error");
});

test("isPhone", () => {
    expect(FormValidator.isPhone("error")("")).toBeUndefined();
    expect(FormValidator.isPhone("error")("777888999")).toBeUndefined();
    expect(FormValidator.isPhone("error")("dev@uxfcz")).toBe("error");
});

test("isLessThan", () => {
    expect(FormValidator.isLessThan(10, "error")("")).toBeUndefined();
    expect(FormValidator.isLessThan(10, "error")("9.9")).toBeUndefined();
    expect(FormValidator.isLessThan(10, "error")("10")).toBe("error");
    expect(FormValidator.isLessThan(10, "error")("100")).toBe("error");
});

test("isLessOrEqualThan", () => {
    expect(FormValidator.isLessOrEqualThan(10, "error")("")).toBeUndefined();
    expect(FormValidator.isLessOrEqualThan(10, "error")("9.9")).toBeUndefined();
    expect(FormValidator.isLessOrEqualThan(10, "error")("10")).toBeUndefined();
    expect(FormValidator.isLessOrEqualThan(10, "error")("10.001")).toBe("error");
});

test("isGreaterThan", () => {
    expect(FormValidator.isGreaterThan(10, "error")("")).toBeUndefined();
    expect(FormValidator.isGreaterThan(10, "error")("9")).toBe("error");
    expect(FormValidator.isGreaterThan(10, "error")("10")).toBe("error");
    expect(FormValidator.isGreaterThan(10, "error")("10.001")).toBeUndefined();
});

test("isGreaterOrEqualThan", () => {
    expect(FormValidator.isGreaterOrEqualThan(10, "error")("")).toBeUndefined();
    expect(FormValidator.isGreaterOrEqualThan(10, "error")("9")).toBe("error");
    expect(FormValidator.isGreaterOrEqualThan(10, "error")("10")).toBeUndefined();
    expect(FormValidator.isGreaterOrEqualThan(10, "error")("10.001")).toBeUndefined();
});
