import { Validator } from "./Validator";

const emptyValue = (value: any) => value === null || value === undefined || value === "";

// required
const required = (errorMessage?: string) => (value: any) =>
    emptyValue(value) ? errorMessage || "Vyplňte." : undefined;

// email
const isEmail = (errorMessage?: string) => (value: string): string | undefined =>
    !emptyValue(value) && !Validator.isEmail(value) ? errorMessage || "E-mail není validní" : undefined;

// phone
const isPhone = (errorMessage?: string) => (value: string): string | undefined =>
    !emptyValue(value) && !Validator.isPhone(value) ? errorMessage || "Telefon není validní" : undefined;

const isLessThan = (maxValue: number, errorMessage?: string) => (value: string) => {
    if (value === "") {
        return undefined;
    }
    const numberValue = Number.parseFloat(value);
    return numberValue >= maxValue ? errorMessage || "Tohle je nějak moc – jste si jisti?" : undefined;
};

// less or equal
const isLessOrEqualThan = (maxValue: number, errorMessage?: string) => (value: string) => {
    if (value === "") {
        return undefined;
    }
    const numberValue = Number.parseFloat(value);
    return numberValue > maxValue ? errorMessage || "Tohle je nějak moc – jste si jisti?" : undefined;
};

// greater
const isGreaterThan = (minValue: number, errorMessage?: string) => (value: string) => {
    if (emptyValue(value)) {
        return undefined;
    }
    const numberValue = Number.parseFloat(value);
    return numberValue <= minValue ? errorMessage || "Tohle je nějak moc – jste si jisti?" : undefined;
};

// greater or equal
const isGreaterOrEqualThan = (minValue: number, errorMessage?: string) => (value: string) => {
    if (emptyValue(value)) {
        return undefined;
    }
    const numberValue = Number.parseFloat(value);
    return numberValue < minValue ? errorMessage || "Tohle je nějak moc – jste si jisti?" : undefined;
};

export const FormValidator = {
    isEmail,
    isGreaterOrEqualThan,
    isGreaterThan,
    isLessOrEqualThan,
    isLessThan,
    isPhone,
    required,
};
