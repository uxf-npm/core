import { RefObject, useCallback } from "react";
import { KeyFilter, useKey } from "./useKey";

export function useKeydownToMouseClick<T extends HTMLElement>(
    ref: RefObject<T>,
    keyFilter: KeyFilter,
    disabled = false,
): void {
    const handler = useCallback(
        (e: KeyboardEvent) => {
            const thisNode = ref.current;
            const currentNode = e.target;
            if (currentNode && currentNode === thisNode && !disabled) {
                e.preventDefault();
                e.stopPropagation();
                thisNode.dispatchEvent(
                    new MouseEvent("click", {
                        view: window,
                        bubbles: true,
                        cancelable: true,
                        buttons: 1,
                    }),
                );
            }
        },
        [disabled, ref],
    );

    useKey(ref, keyFilter, handler, "keydown", disabled);
}
