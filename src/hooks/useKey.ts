import { RefObject, useEffect } from "react";
import { isBrowser } from "../utils/isBrowser";

export type KeyFilter = null | undefined | string | ((e: KeyboardEvent) => boolean);
type KeyPredicate = (e: KeyboardEvent) => boolean;

const createKeyPredicate = (keyFilter: KeyFilter): KeyPredicate =>
    typeof keyFilter === "function"
        ? keyFilter
        : typeof keyFilter === "string"
        ? (e: KeyboardEvent) => e.key === keyFilter
        : keyFilter
        ? () => true
        : () => false;

export function useKey<T extends HTMLElement>(
    ref: RefObject<T>,
    keyFilter: KeyFilter,
    callback: (e: KeyboardEvent) => void,
    type: "keydown" | "keyup" | "keypress",
    disabled = false,
): void {
    useEffect((): (() => void) | void => {
        const thisNode = ref.current;
        if (isBrowser && thisNode && !disabled) {
            const predicate = createKeyPredicate(keyFilter);

            const handler = (e: KeyboardEvent) => {
                if (predicate(e)) {
                    callback(e);
                }
            };

            thisNode.addEventListener(type, handler);
            return () => {
                thisNode.removeEventListener(type, handler);
            };
        }
    }, [callback, disabled, keyFilter, ref, type]);
}
