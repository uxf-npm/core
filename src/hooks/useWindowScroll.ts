import { useEffect } from "react";
import { isBrowser } from "../utils/isBrowser";
import { useRafState } from "./useRafState";

export function useWindowScroll() {
    const [state, setState] = useRafState<{ x: number; y: number } | undefined>(undefined);

    useEffect((): (() => void) | void => {
        if (isBrowser) {
            const handler = () =>
                setState({
                    x: window.pageXOffset,
                    y: window.pageYOffset,
                });
            handler();

            window.addEventListener("scroll", handler, {
                capture: false,
                passive: true,
            });
            return () => {
                window.removeEventListener("scroll", handler);
            };
        }
    }, [setState]);

    return state;
}
