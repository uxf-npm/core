import { useEffect } from "react";
import { isBrowser } from "../utils/isBrowser";
import { useRafState } from "./useRafState";

export function useWindowSize() {
    const [state, setState] = useRafState<{ width: number; height: number } | undefined>(undefined);

    useEffect((): (() => void) | void => {
        if (isBrowser) {
            const handler = () =>
                setState({
                    width: window.innerWidth,
                    height: window.innerHeight,
                });
            handler();

            window.addEventListener("resize", handler);
            return () => {
                window.removeEventListener("resize", handler);
            };
        }
    }, [setState]);

    return state;
}
