import { BodyScrollOptions, clearAllBodyScrollLocks, disableBodyScroll, enableBodyScroll } from "body-scroll-lock";
import { RefObject, useEffect } from "react";

export function useBodyScrollLock<T extends HTMLElement>(
    ref: RefObject<T>,
    isOpen: boolean,
    clearAllOnClose = false,
    options?: Partial<BodyScrollOptions>,
): void {
    useEffect((): (() => void) | void => {
        const targetNode = ref.current;
        if (targetNode) {
            if (isOpen) {
                disableBodyScroll(targetNode, {
                    allowTouchMove: element => {
                        while (element !== document.body) {
                            if (element.getAttribute("data-body-scroll-lock-ignore") !== null) {
                                return true;
                            }
                            element = element.parentElement ?? element;
                        }
                        return;
                    },
                    ...(options || {}),
                });
            }
            return () => {
                enableBodyScroll(targetNode);
            };
        } else if (clearAllOnClose) {
            return () => {
                clearAllBodyScrollLocks();
            };
        }
    }, [clearAllOnClose, isOpen, options, ref]);
}
