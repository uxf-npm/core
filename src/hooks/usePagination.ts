import { useMemo } from "react";
import { PaginationConfig, paginationGenerate, PaginationItem } from "../utils/pagination";

export const usePagination = (config: PaginationConfig): PaginationItem[] => {
    return useMemo(() => paginationGenerate(config), [
        config.boundaryCount,
        config.siblingCount,
        config.count,
        config.page,
        config.hideNextButton,
        config.hidePrevButton,
        config.showFirstButton,
        config.showLastButton,
    ]);
};
