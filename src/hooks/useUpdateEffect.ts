import { useEffect, useRef } from "react";

export function useUpdateEffect(fn: () => void, deps: any[]) {
    const didMountRef = useRef(false);

    useEffect(() => {
        if (didMountRef.current) {
            fn();
        } else {
            didMountRef.current = true;
        }
    }, deps);
}
