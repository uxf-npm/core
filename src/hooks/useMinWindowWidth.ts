import { useEffect } from "react";
import { isBrowser } from "../utils/isBrowser";
import { useRafState } from "./useRafState";

export function useMinWindowWidth(minWidth: number) {
    const [state, setState] = useRafState<boolean | undefined>(undefined);

    useEffect((): (() => void) | void => {
        if (isBrowser) {
            const handler = () => setState(window.innerWidth >= minWidth);
            handler();

            window.addEventListener("resize", handler);
            return () => {
                window.removeEventListener("resize", handler);
            };
        }
    }, [minWidth, setState]);

    return state;
}
