export const queryParamToString = (param?: string | string[]): string => (typeof param === "string" ? param : "");

export const queryParamToNumber = (param?: string | string[]): number=>
    typeof param === "string" ? parseInt(param, 10) : 0;
